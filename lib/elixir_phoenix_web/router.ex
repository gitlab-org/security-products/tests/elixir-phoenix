defmodule ElixirPhoenixWeb.Router do
  use ElixirPhoenixWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash

    # Next line is commented out to trigger following 'sobelow' vulnerability:
    #   'Config.CSRF: Missing CSRF Protections'
    # DOC: https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/csrf.ex
    # plug :protect_from_forgery

    # Leaving next line is "uncommented" without providing a
    # 'Content-Security-Policy' header in the custom headers map to trigger
    # following 'sobelow' vulnerability:
    #   'Config.CSP: Missing Content-Security-Policy'
    # DOC: https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/csp.ex
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ElixirPhoenixWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/bin_to_term_test", PageController, :bin_to_term_test
    get "/ci_system", PageController, :ci_system
    get "/dos_binary_to_atom", PageController, :dos_binary_to_atom
    get "/sql_query", PageController, :sql_query
    get "/traversal_send_download", PageController, :traversal_send_download

    # Next two routes share an action to trigger following 'sobelow'
    # vulnerability:
    #   'Config.CSRFRoute: CSRF via Action Reuse'
    # DOC: https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/csrf_route.ex
    get "/csrf_via_action_reuse", PageController, :csrf_via_action_reuse
    post "/csrf_via_action_reuse", PageController, :csrf_via_action_reuse

    get "/xss", XssController, :xss_aliased_put_content_type
    get "/xss", XssController, :xss_put_content_type
  end

  # Other scopes may use custom stacks.
  # scope "/api", ElixirPhoenixWeb do
  #   pipe_through :api
  # end

  # DOC: https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/headers.ex
  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:protect_from_forgery)
  end
end
