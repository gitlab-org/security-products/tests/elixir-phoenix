use Mix.Config

# DOC: https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/hsts.ex
config :elixir_phoenix, ElixirPhoenixWeb.Endpoint,
  https: [
    :inet6,
    port: 443,
    cipher_suite: :strong,
    keyfile: System.get_env("SOME_APP_SSL_KEY_PATH"),
    certfile: System.get_env("SOME_APP_SSL_CERT_PATH")
  ]
