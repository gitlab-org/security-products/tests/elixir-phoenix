defmodule ElixirPhoenixWeb.XssController do
  use ElixirPhoenixWeb, :controller

  # "vulnerable indirect aliased put_resp_content_type"
  def xss_aliased_put_content_type(conn, %{"test" => test}) do
    put_resp_content_type(conn, ImageMime.get(test))
    |> send_file(200, "file.txt")
  end

  # "vulnerable put_resp_content_type"
  def xss_put_content_type(conn, %{"test" => test}) do
    put_resp_content_type(conn, test)
    |> send_file(200, "file.txt")
  end

  # "vulnerable html"
  def vulnerable_html(conn, %{"test" => test}) do
    html conn, test
  end

  # "default content_type send_resp"
  def xss_send_resp(conn, %{"test" => test}) do
     send_resp(conn, 200, test)
  end
end
